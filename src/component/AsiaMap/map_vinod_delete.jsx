import React, { Component } from "react"
import { geoMercator, geoPath } from "d3-geo"
import { feature } from "topojson-client";
import * as d3 from 'd3';
import worlddata from './Data/asiaTopo.json';
import Asia from './Data/dataAsia.json';
import { Popover, Button } from 'antd';
import 'antd/dist/antd.css';
class WorldMap extends Component {
  constructor(props) {
    super(props);
    this.state = {
      worldData: [],
      countries : {}
}
}

  projection() {
    return geoMercator()
      .scale(400)
      .translate([ -500 / 2, 1000 / 2 ]);
  }
   scale = d3.scaleLinear()
            .domain([-3,3])
            .range(['#003082', '#d9ecf7']);

  componentDidMount() {
    this.setState({
     worldData: feature(worlddata, worlddata.objects.asialates).features,
     countries: Asia[this.props.Indicators]
   })
  }
  onMouseOverCountry = (data) => {
    console.log(data+"hello its working on every country");
  }
componentWillReceiveProps(nexProps){
  this.setState({
    countries: Asia[nexProps.Indicators]
  })
}
changeColor(index){
  if(index >= 0.8){
    return "#004481";
  }else if(index < 0.8 && index >= 0.2){
    return "#77aec7";
  } else if(index < 0.2 && index >= -0.2){
    return "#c7d8e1";
  } else if(index < -0.2 && index >=-0.85){
    return "#d8b643";
  } else if ( index < -0.85 ) {
    return "#e26a09"
  }
}
  render() {
    // console.log(this.state.countries);
    // this.state.worldData.map(d => console.log(d.properties.name))
    return (
    <div>


      <svg width={1000 } height={600 }>

        <g className="countries" transform="translate(-90,-10)">
          {
            this.state.worldData.map((d,i) => (
            // cohesion = d.properties.name

              <Popover content={this.state.countries[this.props.Time][d.properties.name]} style={{marginBottom: '0px'}} title={d.properties.name} trigger="hover">
              <path
                key={ `path-${ i }` }
                d={ geoPath().projection(this.projection())(d) }
                className="country"
                fill={ this.changeColor(this.state.countries[this.props.Time][d.properties.name]) }
                className="country"
                stroke="white"
                strokeWidth={ 0.5 }
                onMouseOver={ () => this.onMouseOverOnCountry}
                style={{stroke: '#000', strokeWidth: 0.5, strokeLinecap: 'round', strokeLinejoin: 'miter', strokeMiterlimit: 4, strokeOpacity: 2, strokeDasharray:'none'}}

              />
            </Popover>
            ))
          }

        </g>

        <g className="markers">
          <circle
            cx={ this.projection()([8,48])[0] }
            cy={ this.projection()([8,48])[1] }
            r={ 10 }
            fill="#E91E63"
            className="marker"
          />
        </g>
      </svg>
      </div>
    )
  }
}

export default WorldMap;
{/* <path
key={ `path-${ i }` }
d={ geoPath().projection(this.projection())(d) }
className="country"
fill={ this.scale(this.state.countries[this.props.Time][d.properties.name]) }
stroke="#FFFFFF"
strokeWidth={ 0.5 }
onMouseOver={ () => this.onMouseOverCountry(d.properties.name)}
/> */}
