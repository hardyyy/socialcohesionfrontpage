import React, { Component } from "react"
import { geoMercator, geoPath } from "d3-geo"
import { feature } from "topojson-client";
import * as d3 from 'd3';
import worlddata from './Data/asiaTopo.json';
import Asia from './Data/dataAsia.json';
import { Popover, Button } from 'antd';
import 'antd/dist/antd.css';
class WorldMap extends Component {
  constructor(props) {
    super(props);

    this.state = {
      hasError: false,
      worldData: [],
      countries : {},
      citiesP:[
        { name: "Hong Kong", coordinates: [101,23.5726], population: 14667000 },
        { name: "Singapore", coordinates: [90.97576600000002,3.210484], population: 14667000 },
      ],
      cities: [
        { name: "Kolkata", coordinates: [61,35.5726], population: 14667000 },
        { name: "Kolkata", coordinates: [62,36], population: 14667000 },
        { name: "Kolkata", coordinates: [64,36.1726], population: 14667000 },
        { name: "Kolkata", coordinates: [64.80,36.6726], population: 14667000 },
        { name: "Kolkata", coordinates: [63,37.5726], population: 14667000 },
        { name: "Kolkata", coordinates: [63.800,36.9726], population: 14667000 },
        { name: "Kolkata", coordinates: [61.00,37.8726], population: 14667000 },
        { name: "Kolkata", coordinates: [59.500,37], population: 14667000 },
        { name: "Kolkata", coordinates: [59.50,35.500], population: 14667000 },
        { name: "Kolkata", coordinates: [59.500,34.500], population: 14667000 },
        { name: "Kolkata", coordinates: [61.00,34], population: 14667000 }, 
        
      ]
}
}

  projection() {
    var w=window,
    d=document,
    e=d.documentElement,
    g=d.getElementsByTagName('body')[0],
    x=w.innerWidth || e.clientWidth || g.clientWidth,
    y=w.innerHeight || e.clientHeight || g.clientHeight;

    console.log("x----"+x+"y----"+y)
    // return geoMercator()
     if(x>811){
       return geoMercator()
       .scale(400)
       .translate([ -500 / 2, 1000 / 2 ]);

      //  .scale(500)
      // .translate([ -600 / 2, 1200 / 2 ])
     } else{
       return geoMercator()
     }
  }
   scale = d3.scaleLinear()
            .domain([-3,3])
            .range(['#003082', '#d9ecf7']);


            componentDidCatch(error, info) {
              console.log("componentDidCatch",this.state.hasError)
              this.setState({ hasError: true });
            

            }

  componentDidMount() {
    this.setState({
     worldData: feature(worlddata, worlddata.objects.asialates).features,
     countries: Asia[this.props.Indicators]
   })
  }
  onMouseOverCountry = (data) => {
    console.log(data+"hello its working on every country");
  }
componentWillReceiveProps(nexProps){
  this.setState({
    countries: Asia[nexProps.Indicators]
  })
}
changeColor(index){
  if(index >= 0.8){
    return "#004481";
  }else if(index < 0.8 && index >= 0.2){
    return "#77aec7";
  } else if(index < 0.2 && index >= -0.2){
    return "#c7d8e1";
  } else if(index < -0.2 && index >=-0.85){
    return "#d8b643";
  } else if ( index < -0.85 ) {
    return "#e26a09"
  }
}
  render() {
     if (this.state.countries[this.props.Time]==undefined || null) {
      return <h1>Something went wrong.</h1>;
    }

     var w=window,
        d=document,
        e=d.documentElement,
        g=d.getElementsByTagName('body')[0],
        x=e.innerWidth ||d.clientWidth||g.clientWidth,
        y =e.innerHeight ||d.clientHeight ||g.clientHeight;

   if(x>811){
    return (
    <div> 
      <svg width={1000 } height={600 }>
        <g className="countries" transform="translate(-90,-10)">
          {
            this.state.worldData.map((d,i) => (
            // cohesion = d.properties.name
 
              <Popover content={this.state.countries[this.props.Time][d.properties.name]} style={{marginBottom: '0px'}} title={d.properties.name} trigger="hover">
              <path
                key={ `path-${ i }` }
                d={ geoPath().projection(this.projection())(d) }
                className="country"
                fill={ this.changeColor(this.state.countries[this.props.Time][d.properties.name]) }
                className="country"
                stroke="white"
                strokeWidth={ 0.5 }
                // onMouseOver = { () => this.onMouseOverOnCountry}
                style={{stroke: 'whtie', strokeWidth: 0.5, strokeLinecap: 'round', strokeLinejoin: 'miter', strokeMiterlimit: 4, strokeOpacity: 2, strokeDasharray:'none'}}
              />
            </Popover>
            ))
          }

        </g>

        <g className="markers">
          <circle
            cx={ this.projection()([8,48])[0] }
            cy={ this.projection()([8,48])[1] }
            r={ 10 }
            fill="#E91E63"
            className="marker"
          />
        </g>
        {/* <g className="markers">
         {
        this.state.cities.map((city, i) => (
          
              <circle
                key={ `marker-${i}` }
                cx={ this.projection()(city.coordinates)[0] }
                cy={ this.projection()(city.coordinates)[1] }
                // r={ city.population / 3000000 }
                r={2}
                fill="white" */}
               
                {/* // stroke="#FFFFFF"
                // className="marker"
                // onMouseOver={ () => this.onMouseOverOnCountry}
                // onClick={ () => this.handleMarkerClick(i) }
              />
        //     ))
        // }
        // </g> */}
        {/* <g className="markers"> */}
         
            {/* <Popover content={this.state.countries[this.props.Time]["Hong kong"]} style={{marginBottom: '0px'}} title={"Hong kong"} trigger="hover"> */}
            {/* {
            this.state.citiesP.map((city, i) => (
              <Popover content={this.state.countries[this.props.Time][city.name]} style={{marginBottom: '0px'}} title={city.name} trigger="hover">
             
              <circle
                key={ `marker-${i}` }
                cx={ this.projection()(city.coordinates)[0] }
                cy={ this.projection()(city.coordinates)[1] }
                r={5}
                fill="red"
                fill={ this.changeColor(this.state.countries[this.props.Time][city.name]) }
                stroke="#FFFFFF"
                className="marker"
                onMouseOver={ () => this.onMouseOverOnCountry}
                 
              /> 
              </Popover>
            ))  }*/}
            
        
        {/* </g> */}
      </svg>
      </div>
    )
  }
    else if(x <780 && x >550){
      return (
      <div> 
        <svg width={1000 } height={350 } style={{marginLeft:"-90%"}}>
          <g className="countries" transform="translate(-90,-10)">
            {
              this.state.worldData.map((d,i) => (
               <Popover content={this.state.countries[this.props.Time][d.properties.name]} style={{marginBottom: '0px'}} title={d.properties.name} trigger="hover">
                <path
                  key={ `path-${ i }` }
                  d={ geoPath().projection(this.projection())(d) }
                  className="country"
                  fill={ this.changeColor(this.state.countries[this.props.Time][d.properties.name]) }
                  className="country"
                  stroke="white"
                  strokeWidth={ 0.5 }
                  style={{stroke: 'whtie', strokeWidth: 0.5, strokeLinecap: 'round', strokeLinejoin: 'miter', strokeMiterlimit: 4, strokeOpacity: 2, strokeDasharray:'none'}}
                />
              </Popover>
              ))
            }
  
          </g>
  
      
        </svg>
        </div>
      )}

      else {
        return (
        <div> 
          <svg width={1000 } height={350 } style={{marginLeft:"-180%"}}>
            <g className="countries" transform="translate(-90,-10)">
              {
                this.state.worldData.map((d,i) => (
                 <Popover content={this.state.countries[this.props.Time][d.properties.name]} style={{marginBottom: '0px'}} title={d.properties.name} trigger="hover">
                  <path
                    key={ `path-${ i }` }
                    d={ geoPath().projection(this.projection())(d) }
                    className="country"
                    fill={ this.changeColor(this.state.countries[this.props.Time][d.properties.name]) }
                    className="country"
                    stroke="white"
                    strokeWidth={ 0.5 }
                    style={{stroke: 'whtie', strokeWidth: 0.5, strokeLinecap: 'round', strokeLinejoin: 'miter', strokeMiterlimit: 4, strokeOpacity: 2, strokeDasharray:'none'}}
                  />
                </Popover>
                ))
              }
    
            </g>
    
            {/* <g className="markers">
              <circle
                cx={ this.projection()([8,48])[0] }
                cy={ this.projection()([8,48])[1] }
                r={ 10 }
                fill="#E91E63"
                className="marker"
              />
            </g>
            <g className="markers">
             {
            this.state.cities.map((city, i) => (
              
                  <circle
                    key={ `marker-${i}` }
                    cx={ this.projection()(city.coordinates)[0] }
                    cy={ this.projection()(city.coordinates)[1] }
                     
                    r={2}
                    fill="white" 
                    className="marker"
                    onMouseOver={ () => this.onMouseOverOnCountry}
                     
                  />
                ))
            }
            </g>
            <g className="markers">
             
                {
                this.state.citiesP.map((city, i) => (
                  <Popover content={this.state.countries[this.props.Time][city.name]} style={{marginBottom: '0px'}} title={city.name} trigger="hover">
                 
                  <circle
                    key={ `marker-${i}` }
                    cx={ this.projection()(city.coordinates)[0] }
                    cy={ this.projection()(city.coordinates)[1] }
                    r={5}
                    fill="red"
                    fill={ this.changeColor(this.state.countries[this.props.Time][city.name]) }
                    stroke="#FFFFFF"
                    className="marker"
                
                  />
                  </Popover>
                ))  }
                
            
            </g> */}
          </svg>
          </div>
        )}


  }
}

export default WorldMap;

