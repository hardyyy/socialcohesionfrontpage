import React from 'react'
import AnyChart from 'anychart-react'
import anychart from 'anychart'

let stage = anychart.graphics.create();

var data = [
    {
        name: "Cohesion", 
        children: [
            {   
                name: "Social relations", 
                children: [
                    {name: "Social Networks"},
                    {name: "Trust in People"},
                    {name: "Acceptance of Diversity"},
                ]
            },
            {
                name: "Connectedness", 
                children: [
                    {name: "Identification"},
                    {name: "Trust in Institutions"},
                    {name: "Perception of Fairness"},
                ]
            },
            {   
                name: "Focus on the Common Good",
                children: [
                    {name: "Civic Participation"},
                    {name: "Respect for Social Rules"},
                    {name: "Solidarity and Helpfulness"}
                ]
            }
        ]
    }
];

// create a chart and set the data
var chart = anychart.sunburst(data, "as-tree");

// set the chart title
chart.title().useHtml(true);
// chart.title("Sunburst: Basic Sample<br><br>" +
//             "<span style='font-size:12; font-style:italic'>" +
//             "Corporate Structure</span>");

// set the container id
chart.container("container");

// initiate drawing the chart
chart.draw();

class Sunburst extends React.Component {
    render() {
        return (
            <section id="about">
            <div class="container">
                <div class="row align-items-center">

                <div class="col-lg-6 col-md-12 col-sm-12">
            	<div class="text-center res_md_mb_30 res_sm_mb_20">
                <AnyChart
                    instance={stage}
                    width={500}
                    height={500}
                    charts={[chart]}
                />
 </div>
            </div>


<div class="col-lg-6 col-md-12 col-sm-12 text_md_center">
                <div class="title_blue_dark">
                  <h4 class="animation" data-animation="fadeInUp" data-animation-delay="0.2s">Social Cohesion Radar  </h4>
                  <p class="animation" data-animation="fadeInUp" data-animation-delay="0.4s" >
                  
                  Social cohesion is understood as the quality of social cooperation and togetherness in a territorially
delimited community. The Social Cohesion Radar (SCR) offers an empirical, international comparison
of social cohesion in various countries worldwide. The SCR is based on a broad set of indicators
drawn from comparative international surveys and other data sources. It breaks down the concept of
social cohesion into three domains — social relations, connectedness and focus on the common good.
Each of these domains comprises three measurable dimensions: social networks, trust in people,
acceptance of diversity, identification, trust in institutions, perception of fairness, solidarity and
helpfulness, respect for social rules, and civic participation.
                  </p>
                  {/* <p class="animation" data-animation="fadeInUp" data-animation-delay="0.8s">
                  Social cohesion is thus understood as the quality
of social cooperation and togetherness in a territorially
delimited community.
                  </p> */}
                </div>
                {/* <a href="https://www.youtube.com/watch?v=ZE2HxTmxfrI" class="btn btn-default btn-radius video animation" data-animation="fadeInUp" data-animation-delay="1s">Let's Start <i class="ion-ios-arrow-thin-right"></i></a>  */}
            </div>



 </div>
    </div>
</section>
        )
    }
}

export default Sunburst;