import React, { Component } from 'react';
import Select from 'react-select';
import {ContinuousColorLegend} from "react-vis";
import './ComponentWrapper.css';
// import './App.css';/
import WorldMap from './AsiaMap';

const options = [
  { value: 'Overallindex', label: 'Overall Index' },
  { value: 'Socialnetwork', label: 'Social Networks' },
  { value: 'Trustinpeople', label: 'Trust in People' },
  { value: 'Acceptanceofdiversity', label: 'Acceptance of Diversity' },
  { value: 'Identification', label: 'Identification' },
  { value: 'TrustinInstitutions', label: 'Trust in Institutions' },
  { value: 'PerceptionOfFairness', label: 'Perception of Fairness' },
  { value: 'SolidarityAndHelpfulness', label: 'Solidarity and Helpfulness' },
  { value: 'RespectForSocialrules', label: 'Respect for Social Rules' },
  { value: 'CivicParticipation', label: 'Civic Participation' },
];

const options1 = [
  {value:'2004-08', label:'2004-08'},
  {value:'2009-15', label:'2009-15'},
]
class App extends Component {
  constructor(props){
    super(props);
    this.state = {
      selectedOption1: [{ value: '2004-08', label : '2004-08'}],
      selectedOption: [{ value: 'Overallindex', label: 'Overall index' }],
    }
  }

  handleChange = (selectedOption) => {
   this.setState({ selectedOption: [selectedOption] });
   console.log(`Option selected:`, selectedOption);
 }
 handleChange1 = (selectedOption1) => {
  this.setState({ selectedOption1: [selectedOption1] });
  console.log(`Option selected:`, selectedOption1);
}

  render() {
    const { selectedOption } = this.state;
    const { selectedOption1 } = this.state;
    return (

  <section id="token"  class="section_token" style={{height: "770px",backgroundColor: "rgba(208, 207, 205, 0.13)"}}>
	<div class="container">
	<div class="row text-center">
      <div class="col-lg-8 col-md-12 offset-lg-2">
        <div class="title_blue_dark text-center">
          <h4 class="animation" data-animation="fadeInUp" data-animation-delay="0.2s">Asia Map Social Cohesion</h4>
          {/* <p class="animation" data-animation="fadeInUp" data-animation-delay="0.4s">The use of crypto-currencies has become more widespread, and
          they are now increasingly accepted .</p> */}
        </div>
      </div>
    </div>
  </div>


  <div class="container" style={{display: "flex"}}>

<div className = "row" className="col-sm-8 inidicatorSelect">

 <WorldMap Time = {selectedOption1[0].value} Indicators = {selectedOption[0].value}/>
 <div className = "ColorLegend">
          <div clasName = "Legend" style = {{"display": "flex" }}>
            <div className ="tier">
              <div className = "circle" style = {{  "background-color": "#004481"}}></div>
              <p>Top-Tier</p>
            </div>
            <div className ="tier">
              <div className = "circle" style = {{  "background-color": "#77aec7"}}></div>
              <p>Mid-Top</p>
            </div>
            <div className ="tier">
              <div className = "circle" style = {{  "background-color": "#c7d8e1"}}></div>
              <p>Mid</p>
            </div>
            <div className ="tier">
              <div className = "circle" style = {{  "background-color": "#d8b643"}}></div>
              <p>Mid-Bottom</p>
            </div>
            <div className ="tier">
              <div className = "circle" style = {{  "background-color": "#e26a09"}}></div>
              <p>Bottom</p>
            </div>
          </div>
        </div>

  </div>


        <div className="col-sm-4 inidicatorSelect" style={{marginTop:"9%"}}>
        <div className="col-sm-12 inidicatorSelect" style={{display: "flex"}}>

         <div className="col-sm-5 inidicatorSelect">
                Choose Year
                <br/>
                <div className="row">
                  <div className="col-sm-12">
                  <Select
                      options={options1}
                      name="Sentiments"
                      className="basic-multi-select"
                      classNamePrefix="select"
                      placeholder = "Indicators"
                      value={selectedOption1}
                      onChange={value => this.handleChange1(value)}
                  />
                </div>
                </div>
            </div>
            <div className="col-sm-7 inidicatorSelect">
                Choose Indicators
                <br/>
                <Select
                    options={options}
                    name="Sentiments"
                    className="basic-multi-select"
                    classNamePrefix="select"
                    placeholder = "Indicators"
                    value={selectedOption}
                    onChange={value => this.handleChange(value)}
                />
            </div>
            </div>

            {/* <div style={{padding:"12%"}}> 


The map displays the country
scores for the overall index as
well as for the nine
dimensions in each of the
examined time periods. The
five colors designate the top
tier (dark blue), second tier
(blue), middle tier (light blue),
fourth tier (yellow), and bottom
tier (orange)
            </div> */}

            <div class="token_rtinfo bg-white border_right">
                	<div class="row text-center" style={{paddingTop:"20%"}}>
                    	<div class="col-lg-12 col-md-12 col-12">
                        	<div class="token_rt_value animation" data-animation="fadeInUp" data-animation-delay="0.2s">
                            	<p> The map displays the country
scores for the overall index as well as for the nine
dimensions in each of the
examined time periods. The
five colors designate the top
tier (dark blue), second tier
(blue), middle tier (light blue),
fourth tier (yellow), and bottom
tier (orange)</p>   
                                {/* <p ref='foo' data-tip={"" +"" + Population.source + ""}>{Population.label}</p> */}
                                {/* <p style={{fontSize:"12px",color: "#a4a4a4"}}>{Population.source+ ""} </p> */}
                            </div>
                        </div>
                        </div>
                        </div>


          </div>





      </div>

</section>
    );
  }
}

export default App;
