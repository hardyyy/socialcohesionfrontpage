import React,{Component} from 'react'


export default class AsiaMap extends React.Component{

    render(){

        return(

            <section id="token" class="section_token">
	<div class="container">
        <div class="row">
			<div class="col-lg-6 offset-lg-3 col-md-12 col-sm-12">
                <div class="title_blue_dark text-center">
                    <h4 class="animation" data-animation="fadeInUp" data-animation-delay="0.2s">Token Sale</h4>
                    <p class="animation" data-animation="fadeInUp" data-animation-delay="0.4s">Join the industry leaders to discuss where the markets are heading. We accept token payments. </p>
                </div>
			</div>
        </div>
        <div class="row">
            <div class="col-lg-12">
            	<div class="blue_bg token_info">
                	<div class="row align-items-center">
                    	<div class="col-lg-5">
                            <div class="text-center">
                                <div class="tk_counter_inner">
                                    <div class="tk_countdown_time animation" data-animation="fadeInUp" data-animation-delay="0.2s" data-time="2019/02/06 00:00:00"></div>
                                    <div class="progress animation" data-animation="fadeInUp" data-animation-delay="0.4s">
                                    <div class="progress-bar progress-bar-striped gradient" role="progressbar" aria-valuenow="46" aria-valuemin="0" aria-valuemax="100" style={{width:"46%"}}> 46% </div>
                                        <span class="progress_label bg-white" style={{left: "30%"}}> <strong> 46,000 BCC </strong></span>
                                        <span class="progress_label bg-white" style={{left: "75%"}}> <strong> 90,000 BCC </strong></span>
                                        <span class="progress_min_val">Sale Raised</span>
                                        <span class="progress_max_val">Soft-caps</span>
                                    </div>
                                    <a href="#" class="btn btn-default btn-radius animation" data-animation="fadeInUp" data-animation-delay="0.6s">Buy Tokens <i class="ion-ios-arrow-thin-right"></i></a>
                                    <ul class="icon_list list_none d-flex justify-content-center">
                                        <li class="animation" data-animation="fadeInUp" data-animation-delay="0.7s"><i class="fa fa-cc-visa"></i></li>
                                        <li class="animation" data-animation="fadeInUp" data-animation-delay="0.8s"><i class="fa fa-cc-mastercard"></i></li>
                                        <li class="animation" data-animation="fadeInUp" data-animation-delay="0.9s"><i class="fa fa-bitcoin"></i></li>
                                        <li class="animation" data-animation="fadeInUp" data-animation-delay="1s"><i class="fa fa-paypal"></i></li>
                                    </ul>                        
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-7">
                        	<div class="token_list_info token_list_shape blue_dark_bg h-100">
                                <div class="row">
                                    <div class="col-md-6 col-sm-6">
                                        <div class="sale_info animation" data-animation="fadeInUp" data-animation-delay="0.2s">
                                            <h6>Private Sale</h6>
                                            <p><span>1 BCC</span> = $32.21 </p>
                                            <p>Bonus <span>65%(0.068)</span></p>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-sm-6">
                                        <div class="sale_info animation" data-animation="fadeInUp" data-animation-delay="0.4s">
                                            <h6>Raised</h6>
                                            <p><span>$ 1.4814.45.24</span></p>
                                            <p>(Completion: 14%)</p>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-sm-6">
                                        <div class="sale_info animation" data-animation="fadeInUp" data-animation-delay="0.6s">
                                            <h6>Public Sale</h6>
                                            <p><span>1 BCC</span> = $12.10</p>
                                            <p>Bonus <span>90% hold Pos</span></p>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-sm-6">
                                        <div class="sale_info animation" data-animation="fadeInUp" data-animation-delay="0.8s">
                                            <h6>Investors</h6>
                                            <p><span>15235</span></p>
                                            <p>Ave. Invest ($): <span>1200.00</span></p>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-sm-6">
                                        <div class="sale_info animation" data-animation="fadeInUp" data-animation-delay="1s">
                                            <h6>Tokens exchange rate</h6>
                                            <p><span>1 ETH</span> = 650 BCC,</p>
                                            <p><span>1 BTC</span> = 1940 BCC</p>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-sm-6">
                                        <div class="sale_info animation" data-animation="fadeInUp" data-animation-delay="1.2s">
                                            <h6>Acceptable Currency</h6>
                                            <p>BTC, ETH, LTC, XRP, BTX, DASH, BTU, TRX</p>
                                        </div>
                                    </div>
                                </div>  
                            </div>                      	
                        </div>
                    </div>
            	  
                </div>
            </div>
        </div>
        <div class="divider large_divider"></div>
        <div class="row">


			<div class="col-lg-6 col-md-12 col-sm-12 res_md_mb_40">
                <div class="title_blue_dark text-center">
                    <h4 class="animation" data-animation="fadeInUp" data-animation-delay="0.2s">Token Sale Proceeds</h4>
                </div>
                <div class="row">
                	<div class="col-lg-12 lg_pt_20 res_sm_pt_0">
                    	<div class="d-flex align-items-center justify-content-center">
                            {/* <div class="animation chart_img" data-animation="fadeInRight" data-animation-delay="0.2s"> 
                                <img  src="assets/images/sale-proceeds5.png" alt="sale-proceeds5" /> 
                            </div> */}


                            {/* <ul class="list_none chart_info_list">
                                <li class="animation" data-animation="fadeInUp" data-animation-delay="0.4s">
                                    <span class="chart_bx color6"></span>
                                    <span>Private/Pre Sale</span>
                                </li>
                                <li class="animation" data-animation="fadeInUp" data-animation-delay="0.5s">
                                    <span class="chart_bx color7"></span>
                                    <span> Public ICO</span>
                                </li>
                                <li class="animation" data-animation="fadeInUp" data-animation-delay="0.6s">
                                    <span class="chart_bx color8"></span>
                                    <span>Team & Advisor</span>
                                </li>
                                <li class="animation" data-animation="fadeInUp" data-animation-delay="0.7s">
                                    <span class="chart_bx color9"></span>
                                    <span>Marketing & General</span>
                                </li>
                                <li class="animation" data-animation="fadeInUp" data-animation-delay="0.8s">
                                    <span class="chart_bx color10"></span>
                                    <span>Bounty</span>
                                </li>
                            </ul> */}




                        </div>
                    </div>
                </div>
			</div>





            <div class="col-lg-6 col-md-12 col-sm-12">
                <div class="title_blue_dark text-center">
                    <h4 class="animation" data-animation="fadeInUp" data-animation-delay="0.2s">Token Distribution</h4>
                </div>
                <div class="row">
                	<div class="col-lg-12 lg_pt_20 res_sm_pt_0">
                    	<div class="d-flex align-items-center justify-content-center">
                            <div class="animation chart_img" data-animation="fadeInRight" data-animation-delay="0.2s"> 
                                <img  src="assets/images/distribution5.png" alt="distribution5" /> 
                            </div>
                            <ul class="list_none chart_info_list">
                                <li class="animation" data-animation="fadeInUp" data-animation-delay="0.4s">
                                    <span class="chart_bx color6"></span>
                                    <span>interconnection Dev</span>
                                </li>
                                <li class="animation" data-animation="fadeInUp" data-animation-delay="0.5s">
                                    <span class="chart_bx color7"></span>
                                    <span>Marketing & General</span>
                                </li>
                                <li class="animation" data-animation="fadeInUp" data-animation-delay="0.6s">
                                    <span class="chart_bx color8"></span>
                                    <span>Mobile Ad Platform</span>
                                </li>
                                <li class="animation" data-animation="fadeInUp" data-animation-delay="0.7s">
                                    <span class="chart_bx color9"></span>
                                    <span>Ad Platform Integration</span>
                                </li>
                                <li class="animation" data-animation="fadeInUp" data-animation-delay="0.8s">
                                    <span class="chart_bx color10"></span>
                                    <span>Operational Overhead</span>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
          </div>
        </div>
    </div>
</section>
        )
    }
}