import React,{Component} from 'react'
import './countryHeader.css'
var Background = "assets/images/rural_8936766565.png";

// style={{backgroundImage: `url(${Background})`}}

// style={{backgroundColor:"blue",paddingTop: "14%"}}
export default class CountryHeaderSection extends React.Component{

    state = {
        data: {}
      }
    
      componentWillReceiveProps(nextProps) {
        // console.log("next props point section", nextProps);
        if (nextProps.data) {
          this.setState({
            ...this.state,
            data: nextProps.data
          })
        }
      }



    render(){
        
 const {data} = this.state

        return(

// {/* <section id="home_section"  style={{backgroundImage: `url(${data.countryImage})`,boxShadow: 'inset 0px 50px 700px rgb(0, 0, 0)'}}  class="section_banner  "> 
 <section id="home_section"  style={{backgroundImage: `linear-gradient(to right, rgba(0,0,0,1), rgba(255,0,0,0)),url(${data.countryImage})`}}  class="section_banner  ">
{/* >>>>>>> abe644ac1f75cd6236b04e3d3d84afbc531256fc */}
    {/* <div class="container"> */}
    <div class="row responsive-row">
        <div class = "col-sm-12 col-md-4 col-lg-4" >
            <div class="banner_text">
                <h1 class = "text-white" >{data.countryName}</h1>
                <p class = "text-white"> 
                {data.countryShortDesc}
                </p>
                
                  
            </div>
        </div>
    </div>
   
        <div class="row align-items-center" >
            <div class="col-lg-6 col-md-12 col-sm-12">
                <div class="col-lg-12 col-md-8  col-sm-12">
                <div class="banner_inner res_md_mb_50 res_xs_mb_30">
                    <div class="tk_countdown transparent_bg tk_border_white rounded-0 text-center " >
                        <div class="banner_text tk_counter_inner banner_text">
                            <div class="tk_countdown_time p-0 transparent_bg box_shadow_none "  data-time="2019/02/06 00:00:00"></div>
                            <div class="progress animation" >
                            <div class="progress-bar progress-bar-striped gradient" role="progressbar" aria-valuenow="46" aria-valuemin="0" aria-valuemax="100" style={{width:17+(data.overAllCohesion +1)*100/2+"%"}}>{data.overAllCohesion}</div>
                                <span class="progress_label bg-white" style={{left: "2%"}}> <strong> -1  </strong></span>
                                <span class="progress_label bg-white " style={{left: "50%"}}> <strong> 0  </strong></span>
                                <span class="progress_label bg-white progressbar" style={{left: (data.overAllCohesion +1)*100/2+"%"}}> <strong>   </strong></span>

                                {/* style={{width:(data.overAllCohesion +1)*100/2+"%"}} */}


                                <span class="progress_label bg-white" style={{left: "98%"}}> <strong> 1  </strong></span>
                               
                                <span class="progress_min_val">Overall index of social cohesion</span>
                                
                                <span class="progress_max_val">2009 - 2015</span>
                            </div>
                        </div>
                	</div>
                </div>
          	</div>

            </div>
           
        </div>
        {/* <div class="row">
        	<div class="col-6">
            	<div class="divider small_divider">
                <div class="banner_inner res_md_mb_50 res_xs_mb_30">
                    <div class="tk_countdown transparent_bg tk_border_white rounded-0 text-center animation" data-animation="fadeIn" data-animation-delay="1.1s">
                        <div class="banner_text tk_counter_inner">
                            <div class="tk_countdown_time p-0 transparent_bg box_shadow_none animation" data-animation="fadeInUp" data-animation-delay="1.2s" data-time="2019/02/06 00:00:00"></div>
                            <div class="progress animation" data-animation="fadeInUp" data-animation-delay="1.3s">
                            <div class="progress-bar progress-bar-striped gradient" role="progressbar" aria-valuenow="46" aria-valuemin="0" aria-valuemax="100" style={{width:"17.5%"}}> -0.65 </div>
                                <span class="progress_label bg-white" style={{left: "2%"}}> <strong> -1  </strong></span>
                                <span class="progress_label bg-white" style={{left: "50%"}}> <strong> 0  </strong></span>
                                <span class="progress_label bg-white" style={{left: "98%"}}> <strong> 1  </strong></span>
                               
                                <span class="progress_min_val">Overall index of social cohesion</span>
                                
                                <span class="progress_max_val">2009 - 2015</span>
                            </div>
                        </div>
                	</div>
                </div>
                </div> 
            </div>
        </div> */}
        <div class="row">
        	<div class="col-12">
            	<div class="divider small_divider"></div> 
            </div>
        </div>
    {/* </div> */}
    
</section>




        )
    }
}