import React, { Component } from 'react';
import Scatter from './ScatterPlot';
import './ComponentWrapper.css';
import Select from 'react-select';

const options = [
  {value :"GDP per capita PPP",label :"GDP per capita PPP"},
  {value :"Net-Income inequality Gini", label :"Net-Income inequality Gini"},
  {value :"HDI-Gender Inequality", label :"HDI-Gender Inequality"},
  {value :"Polity index", label :"Polity index"},
  {value :"KOF Globalization index", label :"KOF Globalization index"},
  {value :"Ethnic Fractionalization", label :"Ethnic Fractionalization"},
  {value :"Language Fractionalization", label :"Language Fractionalization"},
  {value :"Net Migration", label :"Net Migration"},
  {value :"Collective Individualism", label :"Collective Individualism"},
  {value :"Evaluation Life-today", label :"Evaluation Life-today"}
];

class ScatterplotChart extends Component {

  constructor(props){
    super(props);
    this.state = {
      selectedOption: [{ value: 'GDP per capita PPP', label : 'GDP per capita PPP'}],
      value : "GDP per capita PPP",
      countryName: ''
    }
    this.onclick1 = this.onclick1.bind(this);
  }

    handleChange = (selectedOption) => {
        this.setState({ selectedOption: [selectedOption] });
        console.log(`Option selected:`, selectedOption);
    }

    onclick1(e){
        this.setState({
            value : e.target.value
        });
    }

    componentWillMount() {
      // let countryName = this.props.history.location.pathname.split("/")[1];
       let countryName =this.props.countryName;

      console.log("will mount wrapper", countryName);
      this.setState({
        ...this.state,
        countryName
      })
    }

    render() {
        const { selectedOption, countryName } = this.state;
        console.log(this.state.value);

        return (
            // <div className="App">
            //   <h2>Overall index of social cohesion relative to gross domestic product</h2>





<section id="token" class="section_token token_sale overflow_hide">
	<div class="container">
        <div class="row">
			<div class="col-lg-6 offset-lg-3 col-md-12 col-sm-12">
                <div class="title_dark title_border text-center">
                    <h4 class="animation" data-animation="fadeInUp" data-animation-delay="0.2s">Determinants and
                    Outcomes  </h4>
                    {/* <p class="animation" data-animation="fadeInUp" data-animation-delay="0.4s">Join the industry leaders to discuss where the markets are heading. We accept token payments. </p> */}
                </div>
			</div>
        </div>

              <div className = 'row'>
                <div className = "col-md-6 Indicators">
                  <Select
                    options={options}
                    name="Sentiments"
                    className="basic-multi-select"
                    classNamePrefix="select"
                    placeholder = "Indicators"
                    value={selectedOption}
                    onChange={this.handleChange}
                  />
                </div>
                <div style={{paddingLeft:"13%"}}>
                  <Scatter
                    value = {selectedOption[0].value}
                    Indicators = {selectedOption[0].value}
                    countryName={countryName}
                  />
                  </div>
              </div>




                {/* <button onClick = {this.onclick1} value = "GDP per capita PPP">gdp</button>
                <button onClick = {this.onclick1} value = "Net-Income inequality Gini">gini</button>
                <button onClick = {this.onclick1} value = "HDI-Gender Inequality">gihdi</button> */}


            {/* // </div> */}


              </div>
</section>
        );
    }

}

export default ScatterplotChart;
