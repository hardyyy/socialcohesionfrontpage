import React, { Component } from 'react';
import '../../../node_modules/react-vis/dist/style.css'
import {XYPlot,LabelSeries, LineSeries,MarkSeries, VerticalGridLines, HorizontalGridLines, XAxis, YAxis,Hint} from 'react-vis';
import data from './Data/data.json';

let xTitle;
let yTitle = "Social Cohesion Index";

class Scatter extends Component {
  constructor(props){
    super(props);
    this.state = {
      scatter : data["GDP per capita PPP"]["scatter"],
      line : data["GDP per capita PPP"]["line"],
       value: false
    }
  }

  componentWillMount() {
    let incomingScatterData = data[this.props.value]["scatter"]
    let { countryName } = this.props;
    // xTitle = this.props.value;
    // console.log("Will mount", incomingScatterData);
    // this.changeData(incomingScatterData,countryName);
    let newScatterData = [];

    let countryShort = "";

    switch (countryName) {
      case "india":
        countryShort = "IN"
        break;

    }

    incomingScatterData.map((d) => {
      if (d.label === countryShort) {
        d["color"] = 1
      }else {d['color'] = 0 }
        newScatterData.push(d);
    })
    return incomingScatterData;

  }
  changeData(incomingData, country){

    let newScatterData = [];

    let countryShort = "";

    switch (country) {
      case "India":
        countryShort = "IN";
        break;
      case "China":
        countryShort = "CN";
        break;
      case "Indonesia":
        countryShort = "ID";
        break;
      case "Nepal":
        countryShort = "NP";
        break;
      case "Combodia":
        countryShort = "KH";
        break;
      case "Afghanistan":
        countryShort = "AF";
        break;
        case "Bhutan":
        countryShort = "BT";
        break;
        case "Bangladesh":
        countryShort = "BD";
        break;

  }
    incomingData.map((d) => {
      if (d.label === countryShort) {
        d["color"] = 0
      }else {d['color'] = 100 }
        newScatterData.push(d);
    })

    return incomingData;

  }

  componentWillReceiveProps(nextProps){

    let incomingScatterData = data[nextProps.value]["scatter"]

    console.log("Will receive", incomingScatterData);
    let scatterData = this.changeData(incomingScatterData,this.props.countryName);
    this.setState({
      scatter: scatterData,
      line : data[nextProps.value]["line"]
    })
  }


  render() {
    // xTitle = this.props.value;
    if (this.props.value === "Polity index" || this.props.value === "Net Migration" ||  this.props.value === "Evaluation Life-today" ) {
      xTitle = "Social Cohesion Index";
      yTitle = this.props.value;
    } else {
      xTitle = this.props.value;
      yTitle = "Social Cohesion Index";
    }
    console.log(this.state.scatter);
    return (
      <div className="app">
        <XYPlot height={600} width={800}  onMouseLeave={() => this.setState({value: false})} >
          <VerticalGridLines />
          <HorizontalGridLines />
          <XAxis  title={xTitle} position = "middle" style = {{text: {stroke: 'none', fill: '#6b6b76', fontWeight: 600}}}/>
          <YAxis title={yTitle} position = "middle" style = {{text: {stroke: 'none', fill: '#6b6b76', fontWeight: 100}}}/>
          <MarkSeries data = {this.state.scatter}
            size = '5'
            animation
            onValueMouseOver={(datapoint)=>{
                this.setState({value:datapoint})
              }}
               onMouseLeave={() => this.setState({value: false})}
          />
          <LabelSeries data= {this.state.scatter} />
          <LineSeries style={{strokeDasharray: '2  2'}}
             className="second-series"
              data={this.state.line}
              curve={'curveMonotoneX'}>
          </LineSeries>
              {this.state.value ? <Hint value={this.state.value} ></Hint> : null}
        </XYPlot>

      </div>
    );
  }
}

export default Scatter;
