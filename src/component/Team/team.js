import React,{Component} from 'react'



export default class Team extends React.Component{


    render(){

        return(

            
<section id="team">


<div class="container">
	<div class="row text-center">
      <div class="col-lg-8 col-md-12 offset-lg-2">
        <div class="title_blue_dark text-center">
          <h4 class="animation" data-animation="fadeInUp" data-animation-delay="0.2s">Research Team Members</h4>
          {/* <p class="animation" data-animation="fadeInUp" data-animation-delay="0.4s">The use of crypto-currencies has become more widespread, and 
          they are now increasingly accepted .</p> */}
        </div>
      </div>
    </div>
  </div>
    <div class="container">

        
        <div class="row small_space">

        
          <div class="col-lg-3 col-md-6 col-sm-6 res_md_mb_30 res_sm_mb_20">
                <div class="bg_gray4 radius_box team_box_s3 animation animated fadeInUp" data-animation="fadeInUp" data-animation-delay="0.4s" style={{animationDelay: "0.4s; opacity: 1;"}}>
                    <div class="text-center"> 
                    	<img src="assets/images/team_img1.png" alt="team1" />
                    	 
                    </div>
                    <div class="team_info text-center">
                        <h4><a href="#team1" class="content-popup">Peter Walkenhorst</a></h4>
                        <p>Senior Project Manager at Bertelsmann</p>
                    </div>
                    
            </div>
            </div>


 <div class="col-lg-3 col-md-6 col-sm-6 res_md_mb_30 res_sm_mb_20">
                <div class="bg_gray4 radius_box team_box_s3 animation animated fadeInUp" data-animation="fadeInUp" data-animation-delay="0.4s" style={{animationDelay: "0.4s; opacity: 1;"}}>
                    <div class="text-center"> 
                    	<img src="assets/images/team_img1.png" alt="team1" />
                    	 
                    </div>
                    <div class="team_info text-center">
                        <h4><a href="#team1" class="content-popup">Klaus Boehnke</a></h4>
                        <p>Professor at Jacobs University Bremen</p>
                    </div>
                    
            </div>
            </div>


             <div class="col-lg-3 col-md-6 col-sm-6 res_md_mb_30 res_sm_mb_20">
                <div class="bg_gray4 radius_box team_box_s3 animation animated fadeInUp" data-animation="fadeInUp" data-animation-delay="0.4s" style={{animationDelay: "0.4s; opacity: 1;"}}>
                    <div class="text-center"> 
                    	<img src="assets/images/team_img1.png" alt="team1" />
                    	 
                    </div>
                    <div class="team_info text-center">
                        <h4><a href="#team1" class="content-popup">Joshep chan</a></h4>
                        <p>Professor at University of Hong Kong</p>
                    </div>
                    
            </div>
            </div>


             <div class="col-lg-3 col-md-6 col-sm-6 res_md_mb_30 res_sm_mb_20">
                <div class="bg_gray4 radius_box team_box_s3 animation animated fadeInUp" data-animation="fadeInUp" data-animation-delay="0.4s" style={{animationDelay: "0.4s; opacity: 1;"}}>
                    <div class="text-center"> 
                    	<img src="assets/images/team_img1.png" alt="team1" />
                    	 
                    </div>
                    <div class="team_info text-center">
                        <h4><a href="#team1" class="content-popup">Aurel Croissant</a></h4>
                        <p>Professor  at Heidelberg University</p>
                    </div>
                    
            </div>
            </div>











   








        </div>

</div>
</section>

        )
    }
}

