import React,{Component} from 'react'

export default class BreakSection extends React.Component{


    state = {
        data: {}
      }
    
      componentWillReceiveProps(nextProps) {
        // console.log("next props point section", nextProps);
        if (nextProps.data) {
          this.setState({
            ...this.state,
            data: nextProps.data
          })
        }
      }

      render() {

        console.log("point section", this.state.data[0]);
  
        const { data } = this.state;

        return(

<section id="brief" class=" overflow_hide" style ={{background : "#E4ECF1"}}>
	{/* <div class="container"> */}
    	<div class="row no-gutters">
        	<div class="col-lg-12">
            	<div class="title_default_light">
                	<h2  class=" h2Bold">Brief Description </h2>
                    <p>

                     {data.countryShortDesc}   
                    </p>
                    <p>
                    
                   {data.countryBrief}
                     </p>
                     {/* <div class="btn_group animation" data-animation="fadeInUp" data-animation-delay="1.4s"> 
                        <a href="#whitepaper" class="btn btn-default btn-radius page-scroll">Social Cohesion Paper <i class="ion-ios-arrow-thin-right"></i></a> 
                        <a href="#" class="btn btn-border-white btn-radius">Submit <i class="ion-ios-arrow-thin-right"></i></a> 
                    </div> */}

                    {/* <a href="https://www.youtube.com/watch?v=ZE2HxTmxfrI" class="btn btn-default page-scroll video animation" data-animation="fadeInUp" data-animation-delay="0.6s">Let's Start <i class="ion-ios-arrow-thin-right"></i></a> */}
                </div>
            </div>
            {/* <div class="col-lg-8">
            	<div class="row">
                	<div class="col-lg-6">
                    	<div class="work_box res_md_mt_20">
                        	<div class="work_icon animation" data-animation="fadeInUp" data-animation-delay="0.2s">
                        		<i class="ion ion-android-download"></i>
                            </div>
                            <div class="work_inner">
                            	<h4 class="animation" data-animation="fadeInUp" data-animation-delay="0.3s">Download a Wallet</h4>
                                <p class="animation" data-animation="fadeInUp" data-animation-delay="0.4s">Bitcoin is received, stored, and sent using software known as a Bitcoin Wallet. Download the official Bitcoin Wallet for free.</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6">
                    	<div class="work_box res_md_mt_20">
                        	<div class="work_icon animation" data-animation="fadeInUp" data-animation-delay="0.2s">
                        		<i class="ion-ios-locked"></i>
                            </div>
                            <div class="work_inner">
                            	<h4 class="animation" data-animation="fadeInUp" data-animation-delay="0.3s">Safe & Secure</h4>
                                <p class="animation" data-animation="fadeInUp" data-animation-delay="0.4s">We take careful measures to ensure that your bitcoin is as safe as possible. Offline storage provides an important security measure against theft or loss. </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6">
                    	<div class="work_box mt-4">
                        	<div class="work_icon animation" data-animation="fadeInUp" data-animation-delay="0.2s">
                        		<i class="ion ion-android-cart"></i>
                            </div>
                            <div class="work_inner">
                            	<h4 class="animation" data-animation="fadeInUp" data-animation-delay="0.3s">Buy & Sell</h4>
                                <p class="animation" data-animation="fadeInUp" data-animation-delay="0.4s">Blockchain works with exchange partners all around the world to make buying bitcoin in your wallet both a seamless and secure experience.</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6">
                    	<div class="work_box mt-4">
                        	<div class="work_icon animation" data-animation="fadeInUp" data-animation-delay="0.2s">
                        		<i class="ion-android-exit"></i>
                            </div>
                            <div class="work_inner">
                            	<h4 class="animation" data-animation="fadeInUp" data-animation-delay="0.3s">A Better User Interface</h4>
                                <p class="animation" data-animation="fadeInUp" data-animation-delay="0.4s">Bitcoin Core wallet has features most other wallets don't have. But if you don't need them, you can use several other wallets on top of Bitcoin.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div> */}



        </div>
    {/* </div> */}
    {/* <div class="ripple_effect_left_bottom">
    	<div class="circle_bg1">
        	<span></span>
            <span></span>
            <span></span>
            <span></span>
            <span></span>
            <span></span>
        </div>
    </div>
    <div class="ripple_effect_right_top">
    	<div class="circle_bg1">
        	<span></span>
            <span></span>
            <span></span>
            <span></span>
            <span></span>
            <span></span>
        </div>
    </div> */}
</section>
    
        )
    }
}