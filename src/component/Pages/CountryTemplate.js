import React,{Component} from 'react'
import MenuCountry from './../MenuCountry/menu'
import CountryHeaderSection from './../CountryHeaderPage/countryHeader'
import SpiderChart from './../SpiderChart/spiderMap'
import KeyFeagure from './../KeyFeagure/keyFeagure'
 
import ScatterplotChart from './../scatterplot/ComponentWrapper'
import { getAllKeyfeagure} from './../../action/action_keyfeagure'

import {connect} from 'react-redux'
import BreakSection from './../BreakSection/breakSection'
import Footer from './../Footer/footer'
import ComparativeAnalysis from './../ComparativeAnalysis/ComparativeAnalysis'
import Menu from './../Menu/menu'

// background-image: url("assets/images/banner_img3.png");
 class CountryTemplate extends React.Component{

    state = {
        countryDetails:{}
    }


    componentWillMount(){

        // console.log("Path", this.props.location.pathname.split("/")[1]);
        let countryName = this.props.location.pathname.split("/")[1];
        // let CountryData;

        getAllKeyfeagure(countryName)
            .then((data) => {                
                let data2 ='';
                data2 =data;
                this.setState({
                    ...this.state,
                    countryDetails: data
                })
            })
        }



    render(){
        const { countryDetails } = this.state;
 
        if (countryDetails.length > 0) {
            // console.log(this.state.data[0].Title);
            console.log("***********222*********** 222",countryDetails.keyFigures)
          }

        //let {CountryDetails} = this.state;
       

//  console.log("Main PAge ",this.state.CountryDetails.keyFigures[0])
let countryName = this.props.location.pathname.split("/")[1];


        return(
<div>
    
 <MenuCountry /> 


 {/* <Menu /> */}
 <CountryHeaderSection countryName={countryName} data={this.state.countryDetails}/>
 <KeyFeagure  data={this.state.countryDetails.keyFigures}/>
  <div className = 'templateDiv'>
    <ComparativeAnalysis countryName={countryName} data={this.state.countryDetails}/>
    <BreakSection  data={this.state.countryDetails}/>
    <ScatterplotChart countryName={countryName}/>  
  </div>

{/* <Footer /> */}


</div>
        )
    }
}
export default connect(null, {getAllKeyfeagure})(CountryTemplate);