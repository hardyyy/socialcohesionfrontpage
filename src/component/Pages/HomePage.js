import React,{Component} from 'react'

import SunburstChart from './../Sunburst/Sunburst123'

export default class HomPage extends React.Component{


    render(){
        let countryName = this.props.location.pathname.split("/")[1];
        // console.log("countryName",this.props);

        return(
 <div>
     {/* <Menu />
      <MainSection  countryName={countryName}/> */}
      <SunburstChart countryName={countryName}/>
       {/* <AsiaMap countryName={countryName}/>
      <DownloadWhitePaper /> */}
      {/* <RoadMap /> */}
      {/* <FAQ /> */}
      {/* <Team /> */}
      {/* <Contactus />
      <Footer /> */}
 </div>
        )
    }
}