import React,{Component} from 'react'

export default class RoadMap extends React.Component{

render(){
    return(

<section id="roadmap" style={{backgroundColor: "gray"}}>
  {/* <div class="container" style={{backgroundColor: "white"}}>
	<div class="row text-center">
      <div class="col-lg-8 col-md-12 offset-lg-2">
        <div class="title_blue_dark text-center">
          <h4 class="animation" data-animation="fadeInUp" data-animation-delay="0.2s">Cryptocash Roadmap</h4>
          <p class="animation" data-animation="fadeInUp" data-animation-delay="0.4s" style={{color:"white"}}>The use of crypto-currencies has become more widespread, and they are now increasingly accepted as a legitimate currency for transactions.</p>
        </div>
      </div>
    </div>
  </div> */}
   <h4 class="animation" data-animation="fadeInUp" data-animation-delay="0.2s">Social Cohesion Roadmap</h4>
   <br/><br/>
  <div class="container-fluid" style={{backgroundColor: "gray"}}>
        <div class="row roadmap_list small_space align-items-end">
            <div class="col-lg">
                <div class="single_roadmap roadmap_done">
                    <div class="roadmap_icon"></div>
                    <h6 class="animation" data-animation="fadeInDown" data-animation-delay="0.2s">April 2018</h6>
                    <p class="animation" data-animation="fadeInUp" data-animation-delay="0.3s">Inotial Coin Distribution & maketing </p>
                </div>
            </div>
            <div class="col-lg">
                <div class="single_roadmap roadmap_done">
                    <div class="roadmap_icon"></div>
                    <h6 class="animation" data-animation="fadeInUp" data-animation-delay="0.2s">February 2018</h6>
                    <p class="animation" data-animation="fadeInDown" data-animation-delay="0.2s">Exchange Bitcontent to Bitcoin</p>
                </div>
            </div>
            <div class="col-lg">
                <div class="single_roadmap">
                    <div class="roadmap_icon"></div>
                    <h6 class="animation" data-animation="fadeInDown" data-animation-delay="0.2s">March 2018</h6>
                    <p class="animation" data-animation="fadeInUp" data-animation-delay="0.2s">BTCC mode of payment in Bitconcent</p>
                </div>
            </div>
            <div class="col-lg">
                <div class="single_roadmap">
                    <div class="roadmap_icon"></div>
                    <h6 class="animation" data-animation="fadeInUp" data-animation-delay="0.2s">June 2018</h6>
                    <p class="animation" data-animation="fadeInDown" data-animation-delay="0.2s">Send-Receive coin Bitconcent & mobile</p>
                </div>
            </div>
            <div class="col-lg">
                <div class="single_roadmap">
                    <div class="roadmap_icon"></div>
                    <h6 class="animation" data-animation="fadeInDown" data-animation-delay="0.2s">October 2018</h6>
                    <p class="animation" data-animation="fadeInUp" data-animation-delay="0.2s">Coin Marketcap, World Coin Index</p>
                </div>
            </div>
            <div class="col-lg">
                <div class="single_roadmap">
                    <div class="roadmap_icon"></div>
                    <h6 class="animation" data-animation="fadeInUp" data-animation-delay="0.2s">December 2018</h6>
                    <p class="animation" data-animation="fadeInDown" data-animation-delay="0.2s">Online & Trading ICO Token Sale</p>
                </div>
            </div>
        </div>
    </div>
</section>
    )
}



}