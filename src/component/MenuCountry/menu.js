import React,{Component} from 'react'

import { withRouter } from 'react-router';

import Select,{ components } from 'react-select'
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faSortDown, faFastForward  } from "@fortawesome/free-solid-svg-icons";
import { library } from "@fortawesome/fontawesome-svg-core";
import './navbar.css';
const customStyles = {
  control: (base, state) => ({
      ...base,
      // border: "1px solid #002F81",
      borderRadius : "0",
      width : '100%'
  }),
  option: (provided, state) => ({
      ...provided,
      borderBottom: '1px dotted pink',
      padding: 20,
    }),
    dropdownIndicator : (base, state) => ({
        ...base,
        backgroundColor : '#002F81',
    }),
    IndicatorSeparator : () => ({

    })

}

const countryOptions = [
  {"value": "Afghanistan","label": "Afghanistan"},
  {"value": "Bangladesh","label": "Bangladesh"},
  {"value": "Bhutan","label": "Bhutan"},
  {"value": "Cambodia","label": "Cambodia"},
  {"value": "China","label": "China"},
  {"value": "Hong Kong","label": "Hong Kong"},
  {"value": "India","label": "India"},
  {"value": "Indonesia","label": "Indonesia"},
  {"value": "Japan","label": "Japan"},
  {"value": "Lao PDR","label": "Lao PDR"},
  {"value": "Malaysia","label": "Malaysia"},
  {"value": "Myanmar","label": "Myanmar"},
  {"value": "Mongolia","label": "Mongolia"},
  {"value": "Nepal","label": "Nepal"},
  {"value": "Pakistan","label": "Pakistan"},
  {"value": "Philippines","label": "Philippines"},
  {"value": "Sri Lanka","label": "Sri Lanka"},
  {"value": "Singapore","label": "Singapore"},
  {"value": "South Korea","label": "South Korea"},
  {"value": "Taiwan","label": "Taiwan"},
  {"value": "Thailand","label": "Thailand"},
  {"value": "Vietnam","label": "Vietnam"},

];

const DropdownIndicator = props => {
  return (
    <components.DropdownIndicator {...props}>
     <FontAwesomeIcon icon={faSortDown} size="lg"/>
    </components.DropdownIndicator>
  );
};
 
const indicatorSeparatorStyle = {
  alignSelf: 'stretch',
  backgroundColor: "#fff",
  marginBottom: 8,
  marginTop: 8,
  width: 1,
};

const IndicatorSeparator = ({ innerProps }) => {
  return <span style={indicatorSeparatorStyle} {...innerProps} />;
};

class Menu extends React.Component{
  handleCountryDropdown(value) {
console.log("redirect to ", value);
    if(value.value){
  return 
    }
    //  
    //  return this.props.history.push(`/targert`);
}
render(){
return(
<header class="header_wrap" style={{backgroundColor: "#fff"}}>

		<nav class="navbar navbar-expand-lg">
      {/* <div className = "row "> */}
      <div className  = "offset-md-9 col-md-3 col-sm-12 headerSelect">
      <Select
                  placeholder={'Choose Country'}
                  name="chooseCountry"
                  components={{ DropdownIndicator, IndicatorSeparator }}
                  styles={customStyles}
                  options={countryOptions}
                  className="basic-multi-select"
                  classNamePrefix="select"
                  onChange={value =>  this.props.history.push('/'+value.value)}
                />
      </div>
      {/* </div> */}
               








{/* .scrollable-menu {
   style={height: "auto",maxHeight: "200px",overflowX: "hidden"}
    
    
} */}

                {/* <UncontrolledDropdown  inNavbar className="dropRadi">
                    <DropdownToggle nav caret style={{color: "#003082"}}>
                      Choose Country
                    </DropdownToggle>
                    <DropdownMenu right style={{marginRight: "130px"}} style={{height :"auto",maxHeight: "10px",maxHeight: "250px",overflowX: "hidden"}}>

                    <Link to="/Afghanistan">
                        <DropdownItem>
                        Afghanistan
                        </DropdownItem>
                      </Link>
                      <Link to="/Bangladesh">
                        <DropdownItem>
                        Bangladesh
                        </DropdownItem>
                      </Link>
                      <Link to="/Bhutan">
                        <DropdownItem>
                        Bhutan
                        </DropdownItem>
                      </Link>

                      <Link to="/Cambodia">
                        <DropdownItem>
                        Cambodia
                        </DropdownItem>
                      </Link>
                      <Link to="/China">
                        <DropdownItem>
                        China
                        </DropdownItem>
                      </Link>
                      <Link to="/Hong Kong">
                        <DropdownItem>
                          Hong Kong
                        </DropdownItem>
                      </Link>
                      <Link to="/India">
                        <DropdownItem>
                          India
                        </DropdownItem>
                      </Link>
                      <Link to="/Indonesia">
                        <DropdownItem>
                        Indonesia
                        </DropdownItem>
                      </Link>
                      <Link to="/Japan">
                        <DropdownItem>
                        Japan
                        </DropdownItem>
                      </Link>
                      <Link to="/Laos">
                        <DropdownItem>
                        Laos
                        </DropdownItem>
                      </Link>

                      <Link to="/Malaysia">
                        <DropdownItem>
                        Malaysia
                        </DropdownItem>
                      </Link>
                      <Link to="/Mongolia">
                        <DropdownItem>
                        Mongolia
                        </DropdownItem>
                      </Link><Link to="/Myanmar">
                        <DropdownItem>
                        Myanmar
                        </DropdownItem>
                      </Link><Link to="/Nepal">
                        <DropdownItem>
                        Nepal
                        </DropdownItem>
                      </Link>
                      <Link to="/Pakistan">
                        <DropdownItem>
                        Pakistan
                        </DropdownItem>
                      </Link>
                      <Link to="/Philippines">
                        <DropdownItem>
                        Philippines
                        </DropdownItem>
                      </Link>
                      <Link to="/Singapore">
                        <DropdownItem>
                        Singapore
                        </DropdownItem>
                      </Link><Link to="/South Korea">
                        <DropdownItem>
                        South Korea
                        </DropdownItem>
                      </Link><Link to="/Sri Lanka">
                        <DropdownItem>
                        Sri Lanka
                        </DropdownItem>
                      </Link>
                      <Link to="/Taiwan">
                        <DropdownItem>
                        Taiwan
                        </DropdownItem>
                      </Link><Link to="/Thailand">
                        <DropdownItem>
                        Thailand
                        </DropdownItem>
                      </Link>
                      <Link to="/Vietnam">
                        <DropdownItem>
                        Vietnam
                        </DropdownItem>
                      </Link> */}
{/*         
                      <Link to="/Afghanistan">
                        <DropdownItem>
                        Afghanistan
                        </DropdownItem>
                      </Link>
                      <Link to="/Bangladesh">
                        <DropdownItem>
                        Bangladesh
                        </DropdownItem>
                      </Link>
                      <Link to="/Bhutan">
                        <DropdownItem>
                        Bhutan
                        </DropdownItem>
                      </Link>

                      <Link to="/Cambodia">
                        <DropdownItem>
                        Cambodia
                        </DropdownItem>
                      </Link>
                      <Link to="/India">
                        <DropdownItem>
                          India
                        </DropdownItem>
                      </Link>

                      <Link to="/China">
                        <DropdownItem>
                        China
                        </DropdownItem>
                      </Link>
                      <Link to="/Hong Kong">
                        <DropdownItem>
                          Hong Kong
                        </DropdownItem>
                      </Link>
                      <Link to="/Indonesia">
                        <DropdownItem>
                        Indonesia
                        </DropdownItem>
                      </Link>
                      <Link to="/Japan">
                        <DropdownItem>
                        Japan
                        </DropdownItem>
                      </Link> */}

                      {/* <Link to="/Laos">
                        <DropdownItem>
                        Laos
                        </DropdownItem>
                      </Link>

                      <Link to="/Malaysia">
                        <DropdownItem>
                        Malaysia
                        </DropdownItem>
                      </Link>
                      <Link to="/India">
                        <DropdownItem>
                          India
                        </DropdownItem>
                      </Link><Link to="/India">
                        <DropdownItem>
                          India
                        </DropdownItem>
                      </Link><Link to="/India">
                        <DropdownItem>
                          India
                        </DropdownItem>
                      </Link><Link to="/India">
                        <DropdownItem>
                          India
                        </DropdownItem>
                      </Link>
                      <Link to="/India">
                        <DropdownItem>
                          India
                        </DropdownItem>
                      </Link><Link to="/India">
                        <DropdownItem>
                          India
                        </DropdownItem>
                      </Link><Link to="/India">
                        <DropdownItem>
                          India
                        </DropdownItem>
                      </Link><Link to="/India">
                        <DropdownItem>
                          India
                        </DropdownItem>
                      </Link> */}

                      

                       
{/*                       
                    </DropdownMenu>
                  </UncontrolledDropdown> */}
                {/* <ul class="navbar-nav nav_btn align-items-center" style={{marginRight: "76px"}}>
                    <li class="animation" data-animation="fadeInDown" data-animation-delay="1.9s">
                        <div class="lng_dropdown">
                          <select name="countries" id="lng_select"> Country
                        
                          <Link to="/India"> 
                          <option value='en'   data-title="English">India</option> 
                          </Link>
                            
                            <option value='fn'   data-title="France">Afganistan</option>
                            <option value='us'   data-title="United States">Srilanka</option>
                          </select>
                        </div>
                    </li> */}
                    {/* <li class="animation" data-animation="fadeInDown" data-animation-delay="2s"><a class="btn btn-default btn-radius nav_item" href="login.html">Login</a></li> */}
               {/* </ul> */}
			{/* </div> */}
		</nav>
	{/* </div> */}
</header>


)
}

}



export default  withRouter(Menu)