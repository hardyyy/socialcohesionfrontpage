import React from 'react'
import AnyChart from 'anychart-react'
import anychart from 'anychart'

let stage = anychart.graphics.create();

var data = 
[
    {
      "name": "country",
      "children": [
        {
          "name": "Social relations",
          "id": "Social",
          "children": [
            {
                
              "name": "Social Networks",
              "children": [
                {
                  "name": "Biathlon, mixed relay",
                  "value": "1"
                }
              ]
            },
            {
              "name": "Trust in People",
              "value":"1"
            },
            {
                "name": "Acceptance of Diversity",
                "value":"1"
              }
            
          ]
        },
        {
            "name": "Connectedness",
            "id": "Social",
            "children": [
                {
                    
                  "name": "Identification",
                  "children": [
                    {
                      "name": "Identification",
                      "value": "1"
                    }
                  ]
                },
                {
                  "name": "Trust in Institutions",
                  "value":"1"
                },
                {
                    "name": "Perception of Fairness",
                    "value":"1"
                  }
                
              ]
          },
          {
            "name": "Focus on Common Good",
            "id": "Social",
            "children": [
                {
                    
                  "name": "Civic Participation",
                  "children": [
                    {
                      "name": "Biathlon, mixed relay",
                      "value": "1"
                    }
                  ]
                },
                {
                  "name": "Respect for Social Rules",
                  "value":"1"
                },
                {
                    "name": "Solidarity and Helpfulness",
                    "value":"1"
                  }
                
              ]
          }
    ]   
    }
  ]
  
  
  



  
// // create a chart and set the data
var chart = anychart.sunburst(data, "as-tree");

// // set the chart title
// chart.title().useHtml(true);
// // chart.title("Sunburst: Basic Sample<br><br>" +
// //             "<span style='font-size:12; font-style:italic'>" +
// //             "Corporate Structure</span>");

// // set the container id
chart.container("container");
chart.calculationMode('parent-independent');
					chart.sort('asc');
                    chart.level(1).labels().position('circular');
                    chart.level(2).labels().position('circular');
                    chart.level(-1).enabled(false);
                    chart.level(1).labels().position('circular');
                    chart.innerRadius('20%');
                    chart.level(0).thickness('60%');
                    chart.level(2).thickness('40%');
					chart.level(1).thickness('60%');
					chart.level(0).enabled(false);

					// set labels settings
					// chart.labels()
						// set labels position
						// .position('radial')
						// format chart labels
						// .format('{%Name}');

					// format chart tooltip
                    // chart.tooltip().format('Medals: {%leavesSum}');
                    var statistic = chart.getStat();

                    var medalsCount = statistic.levels[statistic.treemaxdepth].sum;

					// create standalone label and set settings
					var centerLabel = anychart.standalones.label();
					centerLabel.enabled(true)
						.text(medalsCount + '\nMedals')
						.width('100%')
						.height('100%')
						.adjustFontSize(true, true)
						.minFontSize(10)
						.maxFontSize(20)
						// .position('center')
						// .anchor('center')
						// .hAlign('center')
						// .vAlign('middle');

					// set label to center content of chart
					chart.center().content(centerLabel);


                    chart.listen('drillChange', function (e) {
						var lastLevel = chart.level(-1);
						var penultimateLevel = chart.level(-2);
						// get leavesSum if drillDown exist
						var medals = (e.current && e.current.node.meta('leavesSum')) || medalsCount;
						// set 2 because first level hidden
						var maxPathLength = 2;

						// if drillDown
						if (e.path.length >= maxPathLength) {
							// show last level
							lastLevel.enabled(true);
							// set settings for penultimate level labels
							penultimateLevel.labels().position('circular');

							// set text for center content
							centerLabel.text(medals + '\nMedals');
						} else {
							// hide last level
							lastLevel.enabled(false);
							// set settings for penultimate level labels
							penultimateLevel.labels().position('circular');

							// set text for center content
							// centerLabel.text(medals + '\nMedals');
						}
					});
			
// // initiate drawing the chart
chart.draw();

class Sunburst extends React.Component {
    render() {
        return (
            <section id="about">
            <div class="container">
                <div class="row align-items-center">

                <div class="col-lg-9 col-md-12 col-sm-12">
            	<div class="text-center res_md_mb_30 res_sm_mb_20">
                <AnyChart
                    instance={stage}
                    width={800}
                    height={800}
                    charts={[chart]}
                />
 </div>
            </div>


<div class="col-lg-3 col-md-12 col-sm-12 text_md_center">
                <div class="title_blue_dark">
                  <h4 class="animation" data-animation="fadeInUp" data-animation-delay="0.2s">Social Cohesion Radar  </h4>
                  <p class="animation" data-animation="fadeInUp" data-animation-delay="0.4s">
                  
                  Social cohesion is understood as the quality of social cooperation and togetherness in a territorially
delimited community. The Social Cohesion Radar (SCR) offers an empirical, international comparison
of social cohesion in various countries worldwide. The SCR is based on a broad set of indicators
drawn from comparative international surveys and other data sources. It breaks down the concept of
social cohesion into three domains — social relations, connectedness and focus on the common good.
Each of these domains comprises three measurable dimensions: social networks, trust in people,
acceptance of diversity, identification, trust in institutions, perception of fairness, solidarity and
helpfulness, respect for social rules, and civic participation.
                  </p>
                  {/* <p class="animation" data-animation="fadeInUp" data-animation-delay="0.8s">
                  Social cohesion is thus understood as the quality
of social cooperation and togetherness in a territorially
delimited community.
                  </p> */}
                </div>
                {/* <a href="https://www.youtube.com/watch?v=ZE2HxTmxfrI" class="btn btn-default btn-radius video animation" data-animation="fadeInUp" data-animation-delay="1s">Let's Start <i class="ion-ios-arrow-thin-right"></i></a>  */}
            </div>



 </div>
    </div>
</section>
        )
    }
}

export default Sunburst;