import React,{Component} from 'react'



export default class Team extends React.Component{


    render(){

        return(

<section id="contact" class="contact_section"  data-image-src="assets/images/contact_bg.png" data-parallax="scroll" >
	<div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12">
            	<div class="title_blue_dark text-center">
                    <h4 class="animation mb-3" data-animation="fadeInUp" data-animation-delay="0.2s">Contact us</h4>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-8 offset-lg-2 col-md-12 pr-0 res_md_pr_15">
            	<div class="lg_pt_20 res_sm_pt_0">
                    <ul class="list_none contact_info info_contact text-center">
                        <li class="animation" data-animation="fadeInUp" data-animation-delay="0.2s"> 
                        	<i class="ion-ios-location"></i>
                            <div class="contact_detail"> <span>Address</span>
                            	<p>Peter Walkenhorst </p>   <p>    
Carl-Bertelsmann-Straße 256 | 33311 Gütersloh | Germany</p>
                            </div>
                        </li>
                        <li class="animation" data-animation="fadeInUp" data-animation-delay="0.5s"> 
                        	<i class="ion-android-call"></i>
                            <div class="contact_detail"> <span>Phone</span>
                            	<p>+49 5241 81-81172</p>
                            </div>
                        </li>
                        <li class="animation" data-animation="fadeInUp" data-animation-delay="0.8s"> 
                        	<i class="ion-ios-email"></i>
                            <div class="contact_detail"> <span>Email-id</span>
                            	<p>peter.walkenhorst@bertelsmann-stiftung.de</p>
                            </div>
                        </li>
                  </ul> 
              	</div>
            </div>
        </div>
        <div class="row justify-content-center">
        	<div class="col-md-10">
            	<div class="lg_pt_20">
            		<div class="row">
                	<div class="col-md-10" style={{marginLeft:"6%"}}>
                        <form method="post" name="enq" class="field_form_s2">
                            <div class="row">
                            	<div class="form-group col-md-6 animation" data-animation="fadeInUp" data-animation-delay="0.4s">
                                	<input type="text" required="required" placeholder="Enter Name *" id="first-name" class="form-control" name="name" />
                             	 </div>
                                <div class="form-group col-md-6 animation" data-animation="fadeInUp" data-animation-delay="0.6s">
                                	<input type="email" required="required" placeholder="Enter Email *" id="email" class="form-control" name="email" />
                                </div>
                                <div class="form-group col-md-6 animation" data-animation="fadeInUp" data-animation-delay="0.8s">
                                	<input type="text" required="required" placeholder="Enter Subject" id="subject" class="form-control" name="subject" />
                                </div>
                                <div class="form-group col-md-6 animation" data-animation="fadeInUp" data-animation-delay="1s">
                                	<input type="text" placeholder="Enter Phone No." id="phone" class="form-control" name="phone" />
                                </div>
                                <div class="form-group col-md-12 animation" data-animation="fadeInUp" data-animation-delay="1.2s">
                                	<textarea required="required" placeholder="Message *" id="description" class="form-control" name="message" rows="3"></textarea>
                                </div>
                                {/* <div class="col-md-12 text-center animation" data-animation="fadeInUp" data-animation-delay="1.4s">
                                	<button type="submit" title="Submit Your Message!" class="btn btn-default btn-block btn-radius" id="submitButton" name="submit" value="Submit">Submit <i class="ion-ios-arrow-thin-right"></i></button>
                                </div> */}

                                <div class="btn_group animation" data-animation="fadeInUp" data-animation-delay="1.4s"> 
                        {/* <a href="#whitepaper" class="btn btn-default btn-radius page-scroll">Social Cohesion Paper <i class="ion-ios-arrow-thin-right"></i></a>  */}
                        <a href="#" class="btn btn-border-white btn-radius">Submit <i class="ion-ios-arrow-thin-right"></i></a> 
                    </div>
                                <div class="col-md-12">
                                	<div id="alert-msg" class="alert-msg text-center"></div>
                                </div>
                            </div>
                        </form>
                    </div>
                    {/* <div class="col-md-5">	
                    	<div id="map" class="contact_map animation res_sm_mt_20" data-animation="fadeInUp" data-animation-delay="1.2s"></div>
                    </div> */}
                </div>
                </div>
            </div>
        </div>
	</div>
</section>
        )
    }
}