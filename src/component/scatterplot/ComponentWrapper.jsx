import React, { Component } from 'react';
import Scatter from './ScatterPlot';
import './ComponentWrapper.css';
import Select,{ components } from 'react-select'
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faSortDown  } from "@fortawesome/free-solid-svg-icons";
import { library } from "@fortawesome/fontawesome-svg-core";
import data from './Data/explanationData.json';

const options = [
  {value :"GDP per capita PPP",label :"Overall index of social cohesion relative to gross domestic product"},
  {value :"Net-Income inequality Gini", label :"Overall index of social cohesion relative to income inequality"},
  {value :"HDI-Gender Inequality", label :"Overall index of social cohesion relative to gender inequality"},
  {value :"Polity index", label :"Overall index of social cohesion relative to polity index"},
  {value :"KOF Globalization index", label :"Overall index of social cohesion relative globalization"},
  {value :"Ethnic Fractionalization", label :"Overall index of social cohesion relative to ethnic fractionalization"},
  {value :"Language Fractionalization", label :"Overall index of social cohesion relative to language fractionalization"},
  {value :"Net Migration", label :"Net migration relative to the overall index of social cohesion"},
  {value :"Collective Individualism", label :"Overall index of social cohesion relative to collectivism-individualism"},
  {value :"Evaluation Life-today", label :"Life satisfaction (present) relative to overall index of social cohesion"}
];
const indicatorSeparatorStyle = {
  alignSelf: 'stretch',
  backgroundColor: "#fff",
  marginBottom: 8,
  marginTop: 8,
  width: 1,
};

const IndicatorSeparator = ({ innerProps }) => {
  return <span style={indicatorSeparatorStyle} {...innerProps} />;
};
const customStyles = {
  control: (base, state) => ({
      ...base,
      borderColor: "#002F81",
      borderRadius : '0'
  }),
  dropdownIndicator : (base, state) => ({
    ...base,
    backgroundColor : '#002F81',
})
}
const DropdownIndicator = props => {
  return (
    <components.DropdownIndicator {...props}>
     <FontAwesomeIcon icon={faSortDown} size="lg"/>
    </components.DropdownIndicator>
  );
};

class ScatterplotChart extends Component {

  constructor(props){
    super(props);
    this.state = {
      selectedOption: [{ value: 'GDP per capita PPP', label : 'Overall index of social cohesion relative to gross domestic product'}],
      value : "GDP per capita PPP",
      countryName: ''
    }
    this.onclick1 = this.onclick1.bind(this);
  }

    handleChange = (selectedOption) => {
        this.setState({ selectedOption: [selectedOption] });
        console.log(`Option selected:`, selectedOption);
    }

    onclick1(e){
        this.setState({
            value : e.target.value
        });
    }

    componentWillMount() {
      // let countryName = this.props.history.location.pathname.split("/")[1];
       let countryName =this.props.countryName;

      console.log("will mount wrapper", countryName);
      this.setState({
        ...this.state,
        countryName
      })
    }

    render() {

      // if (this.props.value === "Polity index" || this.props.value === "Net Migration" ||  this.props.value === "Evaluation Life-today" ) {
      //   xTitle = "Social Cohesion Index";
      //   yTitle = this.props.value;
      // } else {
      //   xTitle = this.props.value;
      //   yTitle = "Social Cohesion Index";
      // }
        const { selectedOption, countryName } = this.state;

        return (
            // <div className="App">
            //   <h2>Overall index of social cohesion relative to gross domestic product</h2>

<section id="determinants" class="section_token token_sale overflow_hide">
	{/* <div class="container"> */}
    <div class="row">
			<div class="col-lg-6 offset-lg-3 col-md-12 col-sm-12">
          <div class="title_dark title_border text-center">
              <h2 >Determinants and
              Outcomes  </h2>
                   
          </div>
			</div>
    </div>
    <div  className = "row layout no-gutters">

        <div className = "col-md-6 Indicators">
          <Select
            options={options}
            name="Sentiments"
            className="basic-multi-select"
            classNamePrefix="select"
            placeholder = "Indicators"
            components={{ DropdownIndicator, IndicatorSeparator }}
            value={selectedOption}
            styles = {customStyles}
            onChange={this.handleChange}
          />
          <br/>
          <div className = "scatterplot" >
          <div className = "scatterplot title" style = {{marginLeft:"40%"}}> <p>{data[selectedOption[0].value]["title"]}</p></div>
            <Scatter
              value = {selectedOption[0].value}
              Indicators = {selectedOption[0].value}
              countryName={countryName}
            />
          </div>
      </div>


      <div className = "col-md-6 explanationDiv" style={{textAlign: "initial",textTransform: "none"}}>
        <div className = "explanation">
        <h2  class=" h2Bold"> {selectedOption[0].label}</h2>
        <p>  {data[selectedOption[0].value]["explained"]} </p>
        {/* <p>Y-axis : </p>

        <p>X-axis : </p> */}
        </div>
      </div>
    </div> 
  {/* </div> */}
</section>
        );
    }

}

export default ScatterplotChart;
