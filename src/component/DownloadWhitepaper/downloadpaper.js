import React,{Component} from 'react'

export default class DownloadWhitePaper extends React.Component{

    render(){
        return(

            <section id="whitepaper" style={{ backgroundColor: "#ffff"}} class="blue_bg wp_pattern">
	<div class="container">
    	<div class="row align-items-center">
            <div class="col-lg-5 col-md-12 col-sm-12">
                <div class="res_md_mb_50 res_sm_mb_20 text_md_center animation" data-animation="fadeInRight" data-animation-delay="0.2s"> 
                    <img src="assets/images/whitepaper3.png" alt="whitepaper3"/> 
                </div>
            </div>
            <div class="col-lg-7 col-md-12 col-sm-12">
              <div class="title_default_light text_md_center">
                <h4 class="animation" data-animation="fadeInUp" data-animation-delay="0.2s">Social Cohesion Study</h4>
                <p class="animation" data-animation="fadeInUp" data-animation-delay="0.4s">
                Social cohesion has become an important public goal in many countries across the globe, not only in the Western hemisphere, but also in Asia. Despite the growing political and academic interest in the concept, there is no generally accepted definition of social cohesion. As a result, empirical insights are lacking. Against this backdrop, the Bertelsmann Stiftung has initiated the “Social Cohesion Radar” which now, for the first time, presents empirical findings on South, Southeast and East Asia. 
             
                </p>
             
               <p class="animation" data-animation="fadeInUp" data-animation-delay="0.6s"> 
               The study provides an analysis and review of social cohesion in 22 Asian countries in a comparative perspective. It presents a valid and reliable measurement of current and past levels of social cohesion and explores its most important determinants and outcomes. As an extension of the Social Cohesion Radar series the study will be of interest and value to policy makers, academics, think tanks and civil society organizations. 
               </p>

               <p class="animation" data-animation="fadeInUp" data-animation-delay="0.8s">
                   All things considered, the Bertelsmann Stiftung's new publication, "What holds Asian Societies Together? Insights from the Social Cohesion Radar", covers much new territory and provides an excellent introduction to the topic. It should be required reading for all observers, commentators and students covering Asia."
               </p>
              
              </div>
              <ul class="dl_lan list_none">
              	{/* <li class="animation" data-animation="fadeInUp" data-animation-delay="0.4s">
                      <a href="https://www.bertelsmann-stiftung.de/en/publications/publication/did/what-holds-asian-societies-together/"><img src="assets/images/eng_flage.png" alt="eng_flage"/>
                      <span>English</span>
                      <i class="fa fa-download"></i> </a> 
                </li> */}
                <div class="btn_group animation" data-animation="fadeInUp" data-animation-delay="1.4s"> 
                        {/* <a href="#whitepaper" class="btn btn-default btn-radius page-scroll">Social Cohesion Paper <i class="ion-ios-arrow-thin-right"></i></a>  */}
                        <a href="https://www.bertelsmann-stiftung.de/en/publications/publication/did/what-holds-asian-societies-together/" class="btn btn-border-white btn-radius">Go To <i class="ion-ios-arrow-thin-right"></i></a> 
                        {/* <a href="https://www.bertelsmann-stiftung.de/en/publications/publication/did/what-holds-asian-societies-together/" /><img src="assets/images/eng_flage.png" alt="eng_flage"/> */}
                     
                    </div>

                {/* <li class="animation" data-animation="fadeInUp" data-animation-delay="0.6s">
                      <a href="#"><img src="assets/images/rus_flage.png" alt="rus_flage"/><span>Russian</span><i class="fa fa-download"></i></a> 
                </li>
                <li class="animation" data-animation="fadeInUp" data-animation-delay="0.8s">
                      <a href="#"><img src="assets/images/chn_flage.png" alt="chn_flage"/><span>Chinese</span><i class="fa fa-download"></i></a> 
                </li>
                <li class="animation" data-animation="fadeInUp" data-animation-delay="1s">
                      <a href="#"><img src="assets/images/frn_flage.png" alt="frn_flage"/><span>French</span><i class="fa fa-download"></i></a>
                </li> */}
              </ul>
            </div>
        </div>
    </div>
</section>
        )


        
    }
}