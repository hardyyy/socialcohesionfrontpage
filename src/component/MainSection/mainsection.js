import React,{Component} from 'react'

var Background = "assets/images/rawpixel-597440-unsplash.jpg";
export default class MainSection extends React.Component{

    render(){
        return(

            // style={{backgroundImage: `url(${Background})`}}

            

    <section id="home_section"  style={{backgroundColor: "rgba(208, 207, 205, 0.13)"}}  class="section_banner banner_section blue_bg banner_rounded_shape">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-lg-6 col-md-12 col-sm-12 order-lg-first">
                <div class="banner_text text_md_center res_md_mb_30 res_xs_mb_20">
                    <h1 class="animation" data-animation="fadeInUp" data-animation-delay="1.1s">What holds Asian Societies together? </h1>
                    <p class="animation" data-animation="fadeInUp" data-animation-delay="1.3s">
                    Economic development and prosperity strengthen social cohesion in Asia, poverty and discrimination against women weaken it.
                     This is the conclusion of a study commissioned by the Bertelsmann Stiftung, which for the ﬁ rst time examined the cohesion of 22 societies in South,
                      Southeast and East Asia (SSEA). According to the study, cohesion is strongest in Hong Kong and Singapore, followed by Thailand and Bhutan.

                    </p>
                    <div class="btn_group animation" data-animation="fadeInUp" data-animation-delay="1.4s"> 
                        {/* <a href="#whitepaper" class="btn btn-default btn-radius page-scroll">Social Cohesion Paper <i class="ion-ios-arrow-thin-right"></i></a>  */}
                        {/* <a href="#" class="btn btn-border-white btn-radius">Social Cohesion Paper <i class="ion-ios-arrow-thin-right"></i></a>  */}
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-md-12 col-sm-12 order-first">
                <div class="banner_image_right banner_img animation" 
                style={{backgroundImage: `url(${Background})`,margin: "-42px",width: "417px", height: "417px",margin: "9px"}}  
                data-animation-delay="1.5s" data-animation="fadeInRight"> 
                    {/* <img alt="banner_vector3" src="assets/images/banner_img3.png" />  */}
                    

{/* style={{backgroundImage: `url(${Background})`,width: "646px", height: "532px"}} */}

                </div>
          	</div>
        </div>
    </div>
    {/* <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Layer_1" x="0px" y="0px" viewBox="0 0 962 96" style="enable-background:new 0 0 962 115;" xml:space="preserve" preserveAspectRatio="none" class="banner_wave">
		<path class="st0" d="M0,0c0,0,100,94,481,95C862,94,962,0,962,0v115H0V0z"></path>
    </svg>  */}
</section>
      
        )
    }
}