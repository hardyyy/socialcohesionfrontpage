import React from 'react';
import Radar from 'react-d3-radar';

const RadarChart = (props,change) => {

  return(

    <Radar 
      width={500}
      height={500}
      padding={70}
      // domainMin={-3.0}
      domainMax={6}
      highlighted={null}
      color= {"#EDC951"}
      onHover={(point) => {
        if (point) {
          console.log('hovered over a data point');
        } else {
          console.log('not over anything');
        }
      }}
      data={{
        variables :props.data.variables,sets:props.data.sets
      }}
    />

  );

}

export default RadarChart;