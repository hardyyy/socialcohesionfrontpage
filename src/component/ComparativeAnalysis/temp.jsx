import React, { Component } from 'react'
import Select from 'react-select'
import RadarChart from './radarchart'
import ReadyNessChart from './readyness'
const NeedsData = require('./default.json');
const ReadinessData = require('./readyness.json');

class NewComparativeAnalysis extends Component {

    constructor(props) {
        super(props);

        this.state = {
            showList: false,
            showList1: false,
            value: [],
            readynessValue: [],
            "StateNeedsData": {
                "variables": NeedsData.variables,
                "sets": NeedsData.sets
            },
            StateReadinessData: {
                "variables": ReadinessData.variables,
                "sets": ReadinessData.sets
            },
            needsCountries: [{value:"India", label: "India"}],
            readinessCountries: [{value:"India", label: "India"}],
            needsCountriesError: '',
            readinessCountriesError: ''
        }

        this.handleNeedsDropdown = this.handleNeedsDropdown.bind(this)
        this.handleReadinessDropdown = this.handleReadinessDropdown.bind(this)
    }

    componentWillMount() {
        
        let page = "India";
        if (page === "India") {
            var length = ReadinessData.sets.length;
            for (var i = 0; i <= length; i++) {
                if (NeedsData.sets[i].key === "India") {
                    let initialNeedsData = NeedsData.sets[i]
                    console.log("endtry", initialNeedsData)
                    this.setState({
                        StateNeedsData: {
                            ...this.state.StateNeedsData,
                            "sets": [initialNeedsData]
                        }
                    })
                    break;
                }
                else {
                    console.log("Else For Loop")
                }
            }
            for (var i = 0; i <= length; i++) {
                if (ReadinessData.sets[i].key === "India") {
                    let initialNeedsData = ReadinessData.sets[i]
                    console.log("endtry", initialNeedsData)
                    this.setState({
                        StateReadinessData: {
                            ...this.state.StateReadinessData,
                            "sets": [initialNeedsData]
                        }
                    })
                    break;
                }
                else {
                    console.log("Else For Loop")
                }
            }
            console.log("Else For Loop", this.state)
        } else {
            console.log("Else For 33333", this.state)
        }
    }


    handleNeedsDropdown(value) {

        let currentNeedsState = NeedsData;
        let newNeedsState = { variables: [], sets: []};
        let newNeedsDropdownValue;
        // let checkCountryPresent = value.find( x => x.value === 'India');

        console.log("current state", currentNeedsState);

        if (value.length < 5) {

            newNeedsDropdownValue = value;

            let tempArrayState = [];

            currentNeedsState.sets.map((country) => {

                let checkCountry = newNeedsDropdownValue.find( x => x.value === country.key);

                if (checkCountry !== undefined) {
                    // console.log("country", country);
                    tempArrayState.push(country);
                }
            })

            newNeedsState.sets = tempArrayState;

            newNeedsState.variables = currentNeedsState.variables;

            this.setState({
                ...this.state,
                StateNeedsData: {
                    variables: newNeedsState.variables,
                    sets: newNeedsState.sets
                },
                needsCountries: newNeedsDropdownValue,
                needsCountriesError: ''
            })

        } else {
            this.setState({
                ...this.state,
                needsCountriesError: 'You can select maximum of 4 countries'
            })
        }

    }

    handleReadinessDropdown(value) {

        let currentReadinessState = ReadinessData;
        let newReadinessState = { variables: [], sets: []};
        let newReadinessDropdownValue;
        // let checkCountryPresent = value.find( x => x.value === 'India');

        console.log("current state", currentReadinessState);

        if (value.length < 5) {
            newReadinessDropdownValue = value;

            let tempArrayState = [];

            currentReadinessState.sets.map((country) => {

                let checkCountry = newReadinessDropdownValue.find( x => x.value === country.key);

                if (checkCountry !== undefined) {
                    // console.log("country", country);
                    tempArrayState.push(country);
                }
            })

            newReadinessState.sets = tempArrayState;

            newReadinessState.variables = currentReadinessState.variables;

            this.setState({
                ...this.state,
                StateReadinessData: {
                    variables: newReadinessState.variables,
                    sets: newReadinessState.sets
                },
                readinessCountries: newReadinessDropdownValue,
                readinessCountriesError: ''
            })

        } else {
            this.setState({
                ...this.state,
                readinessCountriesError: 'You can select maximum of 4 countries'
            })
        }
    }

    render() {

        const { readinessCountries, needsCountries, readinessCountriesError, needsCountriesError } = this.state;

        console.log("needs select state", needsCountries);

        const countryOptions = [
            {"value": "Afghanistan","label": "Afghanistan"},
            {"value": "Bangladesh","label": "Bangladesh"},
            {"value": "Bhutan","label": "Bhutan"},
            {"value": "Cambodia","label": "Cambodia"},
            {"value": "China","label": "China"},
            {"value": "India","label": "India"},
            {"value": "Indonesia","label": "Indonesia"},
            {"value": "Iran, Islamic Rep.","label": "Iran, Islamic Rep."},
            {"value": "Kyrgyz Republic","label": "Kyrgyz Republic"},
            {"value": "Lao PDR","label": "Lao PDR"},
            {"value": "Myanmar","label": "Myanmar"},
            {"value": "Mongolia","label": "Mongolia"},
            {"value": "Nepal","label": "Nepal"},
            {"value": "Pakistan","label": "Pakistan"},
            {"value": "Papua New Guinea","label": "Papua New Guinea"},
            {"value": "Philippines","label": "Philippines"},
            {"value": "Sri Lanka","label": "Sri Lanka"},
            {"value": "Tajikistan","label": "Tajikistan"},
            {"value": "Vietnam","label": "Vietnam"},
            {"value": "Uzbekistan","label": "Uzbekistan"}
        ];

        return (
            <div>
                <div className="ca-temp-card">
                    <div className="container-fluid">
                        <div className="row">
                           
                            <div class="col-sm-6" style={{ backgroundColor: 'white' }}>

                                <div class="col-sm-12" style={{ backgroundColor: '#dcebf4', padding: '15px' }}>
                                    <h3> Challenges </h3>
                                </div>

                                <div className="col-sm-12" style={{ backgroundColor: 'white' }}>
                                    
                                    <br />
                                    
                                    <Select
                                        defaultValue={[{value: "India", label: "India"}]}
                                        isMulti
                                        name="Country"
                                        options={countryOptions}
                                        className="basic-multi-select"
                                        classNamePrefix="select"
                                        value={needsCountries}
                                        onChange={value => this.handleNeedsDropdown(value)}
                                    />

                                    {needsCountriesError}   

                                </div>

                                <div style={{marginTop: '-79px', marginLeft: '-84px'}}>
                                    <RadarChart
                                        data={this.state.StateNeedsData}
                                    />

                                </div>

                                <div className="container">
                                    <div className="row">

                                        {needsCountries.map((d, i) => {
                                            let color;
                                            if (i === 0) {
                                                color = '#1f77b4'
                                            } else if (i === 1) {
                                                color = '#ff7f0e'
                                            } else if (i === 2) {
                                                color = '#2ca02c'
                                            } else {
                                                color = '#d62728'
                                            }
                                            return (
                                                <div 
                                                    className="col-md-4" 
                                                    style={{
                                                        background: `${color}`,
                                                        color: '#fff', 
                                                        borderRadius: '20px',
                                                        margin: '5px'
                                                    }}
                                                >
                                                    {d.value}
                                                </div>
                                            )
                                            
                                        })}

                                    </div>

                                    <div className="row" style={{padding: '10px'}}>
                                        A lower score is better
                                    </div>
                                </div>

                            </div>

                            <div class="col-sm-6" style={{ backgroundColor: 'white' }}>

                                <div class="col-sm-12" style={{ backgroundColor: '#dcebf4', padding: '15px' }}>
                                    <h3> Capacities </h3>
                                </div>

                                <div className="col-sm-12" style={{ backgroundColor: 'white' }}>

                                    <br />

                                    <Select
                                        defaultValue={[countryOptions[0]]}
                                        isMulti
                                        name="Country"
                                        options={countryOptions}
                                        className="basic-multi-select"
                                        classNamePrefix="select"
                                        value={readinessCountries}
                                        onChange={value => this.handleReadinessDropdown(value)}
                                    />

                                    {readinessCountriesError}
                                    
                                </div> 

                                <div style={{marginTop: '-79px', marginLeft: '-50px'}}>
                                    <ReadyNessChart
                                        data={this.state.StateReadinessData}
                                    />
                                    
                                    {readinessCountries.map((d, i) => {
                                        let color;
                                        if (i === 0) {
                                            color = '#1f77b4'
                                        } else if (i === 1) {
                                            color = '#ff7f0e'
                                        } else if (i === 2) {
                                            color = '#2ca02c'
                                        } else {
                                            color = '#d62728'
                                        }
                                        return (
                                            <div 
                                                className="col-md-4" 
                                                style={{
                                                    background: `${color}`,
                                                    color: '#fff', 
                                                    borderRadius: '20px',
                                                    margin: '5px'
                                                }}
                                            >
                                                {d.value}
                                            </div>
                                        )
                                            
                                    })}

                                </div>

                                <div className="row" style={{padding: '10px'}}>
                                    A higher score is better
                                </div>

                            </div>

                        </div>

                    </div>


                </div>

                <div className="row explaination">

                    <div className="col-xs-10 col-sm-5 col-md-4">
                        <h6>WHAT IS BEING SHOWCASED?</h6>
                        <p className="explaination-subtext">
                            A comparative view of the top 3 needs 
                            and readiness indicators versus each 
                            of the other 21 countries 
                        </p>
                    </div>

                    <div className="col-xs-10 col-sm-5 col-md-4">
                        <h6>HOW DID WE ARRIVE AT ABOVE RANKINGS?</h6>
                        <p className="explaination-subtext">
                            The raw scores for each of the 
                            readiness indicators are compared 
                            and visualized using the spider chart below. 
                        </p>
                    </div>

                    <div className="col-xs-10 col-sm-5 col-md-4">
                        <h6>HOW DO THE ABOVE RANKINGS/DATA HELP?</h6>
                        <p className="explaination-subtext">
                            Gives a comparative analysis of the 
                            atlas to help understand how each of 
                            the countries are positioned relative to each other
                        </p>
                    </div>

                </div>
            </div>
        )
    }
}

export default NewComparativeAnalysis;